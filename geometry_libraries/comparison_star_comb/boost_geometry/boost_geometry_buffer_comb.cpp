// Generic Geometry Library - Comb Buffer Comparisons
//
// Copyright Barend Gehrels, 2009, Geodan Holding B.V. Amsterdam, the Netherlands.
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)


#include "../common/starcomb.hpp"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/geometries.hpp>


template <typename Polygon>
void add(Polygon& polygon, double x, double y, int)
{
    typedef typename boost::geometry::point_type<Polygon>::type p;
    boost::geometry::exterior_ring(polygon).push_back(boost::geometry::make<p>(x, y));
}


int main(int argc, char** argv)
{
    try
    {
        bool do_union;
        int testcount, starcount, combcount;
        double factor1, factor2;
        parse(argc, argv, testcount, starcount, combcount, factor1, factor2, do_union);

        typedef boost::geometry::model::d2::point_xy<double> point_type;
        typedef boost::geometry::model::polygon<point_type> polygon_type;
        typedef boost::geometry::model::multi_polygon<polygon_type> multi_polygon_type;

        polygon_type comb;
        make_comb(comb, add<polygon_type>, combcount);

    #ifdef _DEBUG
        std::cout << boost::geometry::wkt(comb) << std::endl;
    #endif


        // Declare strategies
        const double buffer_distance = argc > 3 ? atof(argv[3]) : 9.0 / double(starcount); // good with 101 points


std::cout << "Buffer distance: "  << buffer_distance << std::endl;


        const int points_per_circle = 360;
        boost::geometry::strategy::buffer::distance_symmetric<double> distance_strategy(buffer_distance);
        boost::geometry::strategy::buffer::join_round join_strategy(points_per_circle);
        boost::geometry::strategy::buffer::end_round end_strategy(points_per_circle);
        boost::geometry::strategy::buffer::point_circle circle_strategy(points_per_circle);
        boost::geometry::strategy::buffer::side_straight side_strategy;

        double area = 0;
        int num_points = 0;
        boost::timer t;
        for (int i = 0; i < testcount; i++)
        {
            multi_polygon_type buffered;
            boost::geometry::buffer(comb, buffered,
                distance_strategy, side_strategy,
                join_strategy, end_strategy, circle_strategy);
 
            area += boost::geometry::area(buffered);
            num_points += boost::geometry::num_points(buffered);

#ifdef _DEBUG
           std::cout << boost::geometry::wkt(buffered) << std::endl;
#endif
        }

        report("BOOST_GEOMETRY", area, t, num_points);
    }
    catch(std::exception const& e)
    {
        std::cout << "BOOST_GEOMETRY: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "BOOST_GEOMETRY exception..." << std::endl;
    }

    return 0;
}
