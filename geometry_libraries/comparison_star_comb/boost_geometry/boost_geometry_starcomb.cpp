// Generic Geometry Library - Star Comb Overlay Comparisons
//
// Copyright Barend Gehrels, 2009, Geodan Holding B.V. Amsterdam, the Netherlands.
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)


#include "../common/starcomb.hpp"

#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/geometries.hpp>



template <typename Polygon>
void add(Polygon& polygon, double x, double y, int)
{
    typedef typename boost::geometry::point_type<Polygon>::type p;
    boost::geometry::exterior_ring(polygon).push_back(boost::geometry::make<p>(x, y));
}


#define _DEBUG
int main(int argc, char** argv)
{
    try
    {
        bool do_union;
        int testcount, starcount, combcount;
        double factor1, factor2;
        parse(argc, argv, testcount, starcount, combcount, factor1, factor2, do_union);


        typedef boost::geometry::model::d2::point_xy<double> point_type;
        typedef boost::geometry::model::polygon<point_type> polygon_type;

        polygon_type star, comb;
        make_star(star, add<polygon_type>, starcount, factor1, factor2);
        make_comb(comb, add<polygon_type>, combcount);

    #ifdef _DEBUG
        std::cout << boost::geometry::wkt(star) << std::endl;
        std::cout << boost::geometry::wkt(comb) << std::endl;
    #endif


        double area = 0;
        boost::timer t;
        for (int i = 0; i < testcount; i++)
        {
            std::vector<polygon_type> v;
            if (do_union)
            {
                boost::geometry::union_(star, comb, v);
            }
            else
            {
                boost::geometry::intersection(star, comb, v);
            }

            double a = 0.0;
            for (std::vector<polygon_type>::const_iterator pit = v.begin(); pit != v.end(); ++pit)
            {
                a += boost::geometry::area(*pit);
    #ifdef _DEBUG
                std::cout << boost::geometry::wkt(*pit) << std::endl;
    #endif
            }
            area += a;
        }

        report("BOOST_GEOMETRY", area, t);
    }
    catch(std::exception const& e)
    {
        std::cout << "BOOST_GEOMETRY: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "BOOST_GEOMETRY exception..." << std::endl;
    }

    return 0;
}
