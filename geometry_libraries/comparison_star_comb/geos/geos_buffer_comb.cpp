// Generic Geometry Library - Comb Buffer
//
// Copyright Barend Gehrels, 2009, Geodan Holding B.V. Amsterdam, the Netherlands.
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)


#include "../common/starcomb.hpp"

#include <geos/geom/PrecisionModel.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/Geometry.h>
#include <geos/geom/Point.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/LineString.h>
#include <geos/geom/Polygon.h>
#include <geos/geom/GeometryCollection.h>
#include <geos/geom/Coordinate.h>
#include <geos/geom/CoordinateSequence.h>
#include <geos/geom/CoordinateArraySequence.h>
#include <geos/geom/IntersectionMatrix.h>

#include <geos/util/GeometricShapeFactory.h>
#include <geos/util/GEOSException.h>
#include <geos/util/IllegalArgumentException.h>
#include <geos/opLinemerge.h>
#include <geos/opPolygonize.h>


using namespace geos;
using namespace geos::geom;

GeometryFactory::Ptr global_factory;


void add(CoordinateArraySequence& seq, double x, double y, int)
{
    seq.add(Coordinate(x, y));
}


int main(int argc, char** argv)
{
    bool do_union;
    int testcount, starcount, combcount;
    double factor1, factor2;
    parse(argc, argv, testcount, starcount, combcount, factor1, factor2, do_union);

    PrecisionModel *pm = new PrecisionModel(geos::geom::PrecisionModel::FLOATING);
    GeometryFactory::Ptr global_factory = GeometryFactory::create(pm, -1);
    delete pm;

    Geometry* comb;

    {
        CoordinateArraySequence *comb_seq = new CoordinateArraySequence();
        make_comb(*comb_seq, add, combcount);
        comb = global_factory->createPolygon(global_factory->createLinearRing(comb_seq), NULL);
      std::cout << "Comb, area="  << comb->getArea() << " n="  << comb->getNumPoints() << std::endl;
    }

    const double buffer_distance = argc > 3 ? atof(argv[3]) : 9.0 / double(starcount); // good with 101 points

    double area = 0;
    int num_points = 0;
    boost::timer t;
    for (int i = 0; i < testcount; i++)
    {
        Geometry *g;
        g = comb->buffer(buffer_distance, 90);
        area += g->getArea();
        num_points += g->getNumPoints();
        global_factory->destroyGeometry(g);
    }

    report("GEOS", area, t, num_points);

    //global_factory->destroyGeometry(comb);

    return 0;
}
