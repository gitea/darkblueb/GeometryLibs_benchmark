= Geometry Libraries Benchmark =

##http://svn.osgeo.org/osgeo/foss4g/benchmarking/geometry_libraries
  git.osgeo.org     TBD       OSGeo Sprint November 2020

Maintainers:
 * Barend Gehrels <barend.gehrels -at- geodan.nl>
 * Mateusz Loskot <mateusz -at- loskot.net>

== Overview ==

The benchmarking/geometry_libraries project is dedicated for performance
benchmarks of various Open Source libraries providing implementation of
computational geometry data structures and algorithms.

Originally, the project was developed by Barend Gehrels and Bruno Lalande
in frame of Boost Geometry (aka Generic Geometry Library, GGL) to compare
this library with other available packages.

However, with further development the authors considered it as a broader
test suite and not specific or affiliated with any particular geometry library.
The benchmarking project has moved to OSGeo Subversion repository
and is open for contributions from interested parties and developers.
It is believed that this will allow to improve quality of the performance
comparisons and make it as objective as possible, so trustworthy.

See the project submission to OSGeo Benchmarking:
http://lists.osgeo.org/pipermail/benchmarking/2010-January/000434.html

Mailing list: http://lists.osgeo.org/mailman/listinfo/benchmarking

== Libraries ==

Currently, the project compares the following libraries

 * C++ libraries:
   CGAL, GEOS, GPC, Wykobi, Boost Polygon (gtl), TerraLib, Boost Geometry (GGL)
 * Java libraries: Java Topology Suite (JTS)

Note, the list is open and awaiting further contributions.

== Algorithms ==

The comparison is performed for the following algorithms:

 * area
 * centroid
 * clip (=intersection with rectangle)
 * convex hull
 * intersection
 * simplify
 * within
 
There is also a specific test, 'starcomb', for more challenging
intersections (done with a selection of the mentioned libraries only), 
and 'triangle' (intersecting polygons with 1000ths of triangular holes)

Note, the list is open and awaiting further contributions.


== Download ==

Ssource code is available from OSGeo Subversion repository:

##svn chekcout http://svn.osgeo.org/osgeo/foss4g/benchmarking/geometry_libraries 
   git.osgeo.org   TBD

Test data used by 'comparisons' benchmark can be downloaded from:

##http://www.weather.gov/geodata/catalog/county/html/county100k.htm 
   inluded in this repo    TBD


== Development ==

*** WARNING: DO NOT COMMIT ANY DATA FILES TO THE REPOSITORY ***

== References ==

Computational Geometry Libraries referred by the project:
 * Boost Geometry (aka GGL) - http://trac.osgeo.org/ggl/
 * Boost Polygon (aka GTL) - http://svn.boost.org/svn/boost/sandbox/gtl/
 * Computational Geometry Algorithms Library (CGAL) - http://www.cgal.org/
 * General Polygon Clipper (GPC) - http://www.cs.man.ac.uk/~toby/alan/software/
 * GEOS (Geometry Engine - Open Source) - http://trac.osgeo.org/geos/
 * Java Topology Suite (JTS) - http://tsusiatsoftware.net/jts/
 * TerraLib - http://www.terralib.org/
 * Wykobi - http://www.wykobi.com/
